"use client"

import React from "react";
import infoJSON from "info.json";
import {DataGridPremium, GridToolbar, useGridApiRef, GridApi, useGridApiContext} from "@mui/x-data-grid-premium";
import {Box, ThemeProvider, createTheme, colors} from "@mui/material";
import {objectToColumns, objectToRows} from "../components/dataTransformations";




export default function Page(){
    const [info, setInfo] = React.useState(infoJSON)
    const [primaryGridColumns, setPrimaryGridColumns] = React.useState(objectToColumns(info))
    const [primaryGridRows, setPrimaryGridRows] = React.useState(objectToRows(info))

    console.log(primaryGridColumns)


    function getDetailPanelContent(){
        return(<h1>d</h1>)
    }

    return(
            <>
            <Box sx={{height:"100vh", width:"100%"}}>
            <DataGridPremium columns={primaryGridColumns} rows={primaryGridRows}
                components={{Toolbar: GridToolbar}} checkboxSelection density ="compact" rowReordering={true} experimentalFeatures={{aggregation: true, newEditingApi: true}} getDetailPanelContent={getDetailPanelContent} getDetailPanelHeight={() => "auto"} />
            </Box>
            </>
    )
}