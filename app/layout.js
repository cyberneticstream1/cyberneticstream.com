"use client";
import localFont from '@next/font/local';

import "./globals.css"

import React from "react";
import { useUserAgent } from 'next-useragent';
import {ThemeContext} from "../components/ThemeContext.js"
const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';


const theme = createTheme({
    palette: {
        primary: {
            main: "#000000",
        },
        secondary: {
            main: '#11cb5f',
        },
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }

});

export default function RootLayout({children}) {


    const [uA, setUa] = React.useState({})

  React.useMemo(() => {
      if (typeof window != "undefined") {
          setUa(useUserAgent(window.navigator.userAgent))
      }
    },[])

    // @ts-ignore
    return (
            <html lang="en" className={myFont.className}>
                <head />
                <body>
                    <ThemeContext.Provider value={uA}>
                    <ThemeProvider theme={theme}>
                    {children}
                    </ThemeProvider>
                        </ThemeContext.Provider>

                        </body>

            </html>
            )
}
